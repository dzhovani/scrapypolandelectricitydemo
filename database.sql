-- Schema

CREATE TABLE country (country_id INTEGER primary key autoincrement, name text NOT NULL);
CREATE TABLE indicator (indicator_id INTEGER primary key autoincrement, name text NOT NULL);
CREATE TABLE period (period_id INTEGER primary key autoincrement, name text NOT NULL);
CREATE TABLE data (
      data_id INTEGER primary key autoincrement,
      country_id INTEGER NOT NULL references country(country_id),
      indicator_id INTEGER NOT NULL references indicator(indicator_id),
      period_id INTEGER NOT NULL references period(period_id),
      datetime long NOT NULL,
      value text NOT NULL,
      UNIQUE(country_id, indicator_id, period_id, datetime)
);


-- Data

INSERT INTO country (name) VALUES ('pl');
INSERT INTO period (name) VALUES ('day');        
INSERT INTO period (name) VALUES ('hour');      
INSERT INTO indicator (name) VALUES ('volume');
INSERT INTO indicator (name) VALUES ('consumption');
INSERT INTO indicator (name) VALUES ('price');
