# -*- coding: utf-8 -*-
import datetime as dt
import scrapy


class ElectricityVolumesPolandSpider(scrapy.Spider):
    name = 'volume-poland'

    def __init__(self, date=None):
        self.date = dt.date(*map(int, date.split('-', 3)))

    def start_requests(self):
        yield scrapy.Request(
            'http://wyniki.tge.pl/en/wyniki/rdn/ceny-sredniowazone/?date=%s'
            % self.date
        )

    def parse(self, response):
        table, = response.css('#tabbed>table.t-02')
        total_row, = table.xpath(
            'tbody/tr[@class="total"]'
            '[*[1][span/text()[normalize-space(.)="Total volume"]]]'
        )
        assert total_row.xpath('*[1][self::td and @colspan=2]')
        assert total_row.xpath('*[position()>1][not(@colspan)]')
        head, = table.xpath('thead/tr')
        assert head.xpath('*[1][self::th and @colspan=2]')
        assert head.xpath('*[position()>1][not(@colspan)]')

        for d_tot in total_row.xpath('*[position()>1]'):
            vol, = d_tot.xpath('span/text()').extract()
            vol = float(vol.replace(' ', ''))

            col, = d_tot.xpath('count(preceding-sibling::*|self::*)').extract()
            col = int(float(col))
            day, = head.xpath('*[position()=%d]/span/text()' % col).extract()
            day = dt.date(self.date.year, *map(int, day.split('/')[::-1]))

            yield dict(country='pl', indicator='volume',
                       period='day', datetime=day,
                       value=vol)
