# -*- coding: utf-8 -*-
import datetime as dt
import scrapy


class ElectricityPricesPolandSpider(scrapy.Spider):
    name = 'prices-poland'

    def __init__(self, date=None, earliest_date=None):
        self.date = dt.date(*map(int, date.split('-', 3)))
        self.earliest_date = dt.date(*map(int, earliest_date.split('-', 3)))
        # the task is given with end to start periods
        # that's why the spider is implemented in similar manner
        assert self.date > self.earliest_date

    def start_requests(self):
        while True:
            date = self.date
            yield scrapy.Request(
                'https://wyniki.tge.pl/en/wyniki/euroindex/spot/?date=%s'
                % date
            )
            self.date = self.date - dt.timedelta(7)
            if self.date < self.earliest_date:
                break

    def parse(self, response):
        # the execution is asynchronous and self.date could not be trusted
        # the date in the response._url query string is used instead
        date_str = response._url.split('=')[1]
        end_date = dt.datetime.strptime(date_str, '%Y-%m-%d')
        start_date = end_date - dt.timedelta(7)
        day = dt.date(start_date.year, start_date.month, start_date.day)
        
        # maybe I didn't use [position() > 1] correctly
        # dropped the first item of the row in the python code instead
        table_row = response.xpath('//table[@class="t-02"]//tr[@class="prices"][1]/td/span[@class="wrap"]/text()')[1:].extract()
        #self.log('table row')
        #self.log(table_row)
        for item in table_row:
            day = day + dt.timedelta(1)
            price = float(item)
            if day >= self.earliest_date:
                yield dict(country='pl', indicator='price',
                           period='day', datetime=day,
                           value=price)
