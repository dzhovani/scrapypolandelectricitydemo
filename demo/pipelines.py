# -*- coding: utf-8 -*-
import sqlite3
from contextlib import closing
import scrapy


class Sqlite3Pipeline(object):

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.get('DBPATH'))

    def __init__(self, dbpath):
        self.conn = sqlite3.connect(dbpath, isolation_level=None)

    def process_item(self, item, spider):
        with closing(self.conn.cursor()) as cur:
            cur.execute(
                '''
                INSERT OR IGNORE
                INTO data (country_id, indicator_id, period_id, datetime, value)
                VALUES (
                    (SELECT country_id FROM country WHERE name = ?),
                    (SELECT indicator_id FROM indicator WHERE name = ?),
                    (SELECT period_id FROM period WHERE name = ?),
                    strftime('%s', ?),
                    ?
                );
                ''',
                (item['country'], item['indicator'], item['period'],
                 item['datetime'].isoformat(), item['value'])
            )
            return item
