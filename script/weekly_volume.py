import os
import datetime as dt

import sqlite3
from contextlib import closing



DBPATH = os.path.expanduser('~/demo.db')
QUERY = \
'''
select max(value), sum(value)
from data
where period_id = (select period_id from period where name = 'day')
and country_id = (select country_id from country where name = 'pl')
and indicator_id = (select indicator_id from indicator where name = 'volume')
and datetime > strftime('%s', ?)
and datetime <= strftime('%s', ?)
'''



def main():
    today = dt.date.today() #dt.date(2017, 6, 7)
    mid_date = today - dt.timedelta(7)
    previous_week = mid_date - dt.timedelta(7)
    conn = sqlite3.connect(DBPATH, isolation_level=None)
    with closing(conn.cursor()) as cur:
        cur.execute(QUERY, (mid_date.isoformat(), today.isoformat()))
        maximum, total = cur.fetchone()
        cur.execute(QUERY, (previous_week.isoformat(), mid_date.isoformat()))
        maximum2, total2 = cur.fetchone()
        print('Maximum amount consumed for the day is {maximum} MW/H. The total amount for the week is {total}.'.format(maximum=maximum, total=total))
        print in comparison, last week the values were {maximum2} and {total2} respectively.'.format(maximum2=maximum2, total2=total2))



if __name__ == "__main__":
    main()